/* Common includes and defines for UDP, TCP and T/TCP
 * clients and servers */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define REQUEST 400  
#define REPLY   400

#define UDP_SERV_PORT  7777    /* UDP server's well-known port */
#define TCP_SERV_PORT  8888    /* TCP server's well-known port */
#define TTCP_SERV_PORT 9999    /* T/TCP server's well-know port */

#define SA struct sockaddr *

void err_quit(const char *x) {
    perror(x);
    exit(1);
}

void err_sys(const char *x) {
    perror(x);
}

int read_stream(int, char *, int);
