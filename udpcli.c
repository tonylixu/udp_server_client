#include "cliserv.h"

int main(int argc, char *argv[]) {
    struct sockaddr_in serv;
    struct in_addr addr;
    char request[REQUEST], reply[REPLY];
    socklen_t socket_length;
    int sockfd;                              /* Socket identifier */
    int n;                                   /* Number of bytes received */

    if (argc != 2)
        err_quit("Usage: udpserv <IP Address of Server>");

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 )
        err_quit("Socket creation failed");

    /* Initialize socket structure */
    memset(&serv, 0, sizeof(serv));
    serv.sin_family      = AF_INET;                   /* Set family type */
    serv.sin_port        = htons(UDP_SERV_PORT);      /* Set server port */
    if (inet_aton(argv[1], &addr) == 0)
        err_quit("Invalid IP address");
    serv.sin_addr.s_addr = addr.s_addr;        /* Set server IP address */

    strcpy(request, "This is servent1...");

    socket_length = sizeof(serv);
    if ((sendto(sockfd, request, REQUEST, 0, (struct sockaddr *) &serv, socket_length)) != REQUEST)
        err_sys("sendto error");

    printf("Sending message [%s] to server", request);

    if ((recvfrom(sockfd, reply, REPLY, 0, (struct sockaddr *) &serv, &socket_length)) < 0 )
        err_sys("recvfrom error");

    printf("\nReceived from server: %s", reply);
    exit(0); 
}
