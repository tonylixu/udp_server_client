#include "cliserv.h"

int main() {

    struct sockaddr_in serv, cli;
    char request[REQUEST], reply[REPLY];
    socklen_t cli_addr_len;
    int sockfd, n;

    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
        err_quit("Socket creation error");

    /* Since serv is not declared static, it won't be initialized */
    /* everytime, so we clear it each time the function is run */ 
    memset(&serv, 0, sizeof(serv));
    serv.sin_family = AF_INET;                        /* Set address family */
    serv.sin_port = htons(UDP_SERV_PORT);              /* Set port in network byte order */
    serv.sin_addr.s_addr = htonl(INADDR_ANY);         /* Set INET address in byte order */

    /* Now bind the socket to the port */
    if (bind(sockfd, (struct sockaddr *) &serv, sizeof(serv)) < 0)
        err_quit("Socket bind error");
   
    printf("Server binded to port %d...\n", UDP_SERV_PORT);
    printf("Server binded address %d...\n", INADDR_ANY);
    
    /* Read packets from connected client */
    for (;;) {
        fflush(stdout);                       /* Force a write of all buffered data */
        printf("Waiting for data...\n");
        cli_addr_len = sizeof(cli);
        if ((n = recvfrom(sockfd, request, REQUEST, 0, (struct sockaddr *) &cli, &cli_addr_len)) < 0 ) 
            err_sys("recvfrom error");

        printf("Receivd: %s\n", request);
        
        /* Send confirm message to client */
        strcpy(reply, "[Got your message!]");

        if (sendto(sockfd, reply, REPLY, 0, (struct sockaddr *) &cli, sizeof(cli)) != REPLY)
            err_sys("sendto error");
   
        printf("Sending %s to client\n", reply);
    }
    close(sockfd);
    return 0;
}
